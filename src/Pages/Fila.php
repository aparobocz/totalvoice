<?php

namespace TotalVoice\Http;

use TotalVoice\Socket\Socket;

class Fila extends Socket
{
    /**
     * @param $id
     * @return string
     */
    public function fila_get($id)
    {
        return $this->send_get('/fila/' . $id);
    }

    /**
     * @param $id
     * @param $data
     * @return string
     */
    public function fila_post($id, $data)
    {
        return $this->send_post('/fila/' . $id, $data);
    }

    /**
     * @param $id
     * @param $data
     * @return string
     */
    public function fila_put($id, $data)
    {
        return $this->send_put('/fila/' . $id, $data);
    }

    /**
     * @param $id
     * @param $ramal_id
     * @return string
     */
    public function fila_delete($id, $ramal_id)
    {
        return $this->send_delete('/fila/' . $id . '/' . $ramal_id);
    }
}