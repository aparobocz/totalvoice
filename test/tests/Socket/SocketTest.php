<?php

namespace TotalVoice\Socket;

use TotalVoice\Http\Fila;

class SocketTest extends \PHPUnit\Framework\TestCase
{
    public function testSendGet()
    {
        $fila = new Fila();

        $actual = $fila->fila_get(1);;


        $expected = '
        {
          "status": 200,
          "sucesso": true,
          "motivo": 0,
          "mensagem": "dados retornados com sucesso",
          "dados": {
            "id": 1,
            "nome": "Suporte",
            "chamadas": 0,
            "completado": 0,
            "cancelado": 0,
            "tempo_falado": "00:00:00",
            "tempo_espera": "00:00:00",
            "ramais": [
              {
                "id": 17,
                "nome": "Teste Atendimento",
                "ramal": "4000",
                "login": "ramal@empresa.com.br",
                "prioridade": "0",
                "qtd_ligacao_atendida": "0",
                "ultima_chamada": "0",
                "em_ligacao": false,
                "status": "indisponivel",
                "em_pausa": false,
                "razao_pausa": "",
                "bina": null,
                "tempo_status": "04:38:36"
              }
            ]
          }
        }
        ';

        $this->assertEquals($expected, $actual);
    }

    public function testSendPost()
    {
        $fila = new Fila();

        $data_post = array(
            'id' => 123,
            'ramal_id' => 321
        );

        $actual = $fila->fila_post(123, $data_post);


        $expected = '
        {
          "status": 200,
          "sucesso": true,
          "motivo": 0,
          "mensagem": "Ramal adicionado com sucesso na fila",
          "dados": {
            "id": 4921
          }
        }
        ';

        $this->assertEquals($expected, $actual);
    }

    public function testSendPut()
    {
        $fila = new Fila();

        $data_post = array(
            'id' => 123,
            'ramal_id' => 321
        );

        $actual = $fila->fila_put(123, $data_post);


        $expected = '
        {
          "status": 200,
          "sucesso": true,
          "motivo": 0,
          "mensagem": "dados atualizados com sucesso",
          "dados": null
        }
        ';

        $this->assertEquals($expected, $actual);
    }

    public function testSendDelete()
    {
        $fila = new Fila();

        $actual = $fila->fila_delete(123, 321);;

        $expected = '
        {
          "status": 200,
          "sucesso": true,
          "motivo": 0,
          "mensagem": "Ramal removido da fila com sucesso"
        }
        ';

        $this->assertEquals($expected, $actual);
    }
}