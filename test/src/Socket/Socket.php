<?php

namespace TotalVoice\Socket;

class Socket
{
    private $url = 'api.totalvoice.com.br';
    private $fp, $sock;
    private $port;
    private $host;

    public function __construct()
    {
        $this->fp = fsockopen($this->url, 80, $errno, $errstr, 30);
        $this->sock = socket_create (AF_INET, SOCK_STREAM, 0) or die ('Erro create');
        $this->port = getservbyname ('www', 'tcp');
        $this->host = gethostbyname ($this->site);
        $this->conn = socket_connect ($this->sock, $this->host, $this->port) or die ('Erro ao conectar');
    }

    /**
     * @param $data
     * @return string
     */
    public function send_post($path, $data)
    {
        return $this->getType('POST', $path, $data);
    }

    /**
     * @param $data
     * @return string
     */
    public function send_put($path, $data)
    {
        return $this->getType('PUT', $path, $data);
    }

    /**
     * @param $data
     * @return string
     */
    public function send_get($path)
    {
        return $this->getType('GET', $path);
    }

    /**
     * @param $data
     * @return string
     */
    public function send_delete($path)
    {
        return $this->getType('DELETE', $path);
    }

    /**
     * @return string
     */
    public function getSocketPostFila()
    {
        // FIla: Filas de atendimento
        $data = array(
            'id' => 123,
            'ramal_id' => 123
        );
        $this->send_post_fila($data);
        return "meu socket";
    }

    /**
     * @param $type, POST or PUT
     * @param $path, redirect for data in endpoint
     * @param $data, ARRAY - fields, when GET is empty
     * @return string
     */
    public function getType($type, $path, $data = array())
    {
        $data = http_build_query($data);
        $type = strtoupper($type);

        if ($type == 'POST' or $type == 'PUT') {
            $packet = "$type $path HTTP/1.1\r\n";
            $packet .= "Host: $this->host:$this->port\r\n";
            $packet .= "Referer: $this->url\r\n";
            $packet .= "User-Agent: " . $_SERVER['HTTP_USER_AGENT'] . "\r\n";
            $packet .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $packet .= "Content-Length: " . strlen($data) . "\r\n\r\n";
            $packet .= "$data\r\n";

            socket_write($this->sock, $packet, strlen($packet));
            $out = "<pre><xmp>";
            while ($out = socket_read($this->sock, 2048)) {
                $out .= $out;
            }
            $out .= "</xmp></pre>";
            socket_close($this->sock);

            $output = explode('{"', $out);
            $output = $output[1];
        } else if ($type == 'GET' or $type == 'DELETE') {
            $packet  = "GET {$path} HTTP/1.0\r\n";
            $packet .= "Host: {$this->host}\r\n";
            $packet .= "Connection: close\r\n\r\n";

            if (!($sock = fsockopen($this->host, 80)))
                die( "\n[-] Sem responsta com o servidor: {$this->host}:80\n");

            fwrite($sock, $packet);
            $stramGetContents = stream_get_contents($sock);

            $output = explode('{"', $stramGetContents);
            $output = $output[1];
        } else {
            $output = '"status": 404, "msg": "Verbs HTTP not found" }';
        }


        return '{"' . $output;
    }
}