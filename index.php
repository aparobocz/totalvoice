<?php

use TotalVoice\Http\Fila;

require_once "vendor/autoload.php";

$fila = new Fila();

$tipo = 'get';

if ($tipo == 'get') {
    echo $fila->fila_get(123);
}

if ($tipo == 'post') {
    $data_post = array(
        'id' => 123,
        'ramal_id' => 321
    );
    echo $fila->fila_post(123, $data_post);
}

if ($tipo == 'put') {
    $data_put = array(
        'id' => 123,
        'ramal_id' => 321
    );
    echo $fila->fila_put(123, $data_put);
}

if ($tipo == 'delete') {
    echo $fila->fila_delete(123, 321);
}